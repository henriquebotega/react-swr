import useSWR from "swr";
import api from "../services/api";

const useFetch = (url) => {
	const { data, error, mutate } = useSWR(
		url,
		async (res) => {
			const resp = await api.get(res);
			return resp.data;
		},
		{ revalidateOnReconnect: true }
	);

	return { data, error, mutate };
};

export default useFetch;
