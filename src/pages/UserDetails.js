import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import useFetch from "../hooks/useFetch";

const UserDetails = () => {
	const { id } = useParams();
	// const [user, setUser] = useState();

	// useEffect(() => {
	// 	fetch("http://localhost:3333/users/" + id).then((res) => {
	// 		res.json().then((data) => {
	// 			setUser(data);
	// 		});
	// 	});
	// }, [id]);

	const { data: user } = useFetch("/users/" + id);

	if (!user) {
		return <p>Carregando...</p>;
	}

	return (
		<div>
			<ul>
				<li>ID: {user?.id}</li>
				<li>Name: {user?.name}</li>
			</ul>
		</div>
	);
};

export default UserDetails;
