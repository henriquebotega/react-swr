import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";
import useFetch from "../hooks/useFetch";
import api from "../services/api";
import { mutate as mutateGlobal } from "swr";

const UserList = () => {
	// const [users, setUsers] = useState([]);

	// useEffect(() => {
	// 	fetch("http://localhost:3333/users").then((res) => {
	// 		res.json().then((data) => {
	// 			setUsers(data);
	// 		});
	// 	});
	// }, []);

	const { data: users, mutate } = useFetch("/users");

	const handleNameChange = useCallback(
		(id) => {
			const novoNome = "Bartolomeu";

			api.put("/users/" + id, { name: novoNome });

			const newUsers = users.map((u) => {
				if (u.id === id) {
					return { ...u, name: novoNome };
				}

				return u;
			});

			mutate(newUsers, false);
			mutateGlobal("/users/" + id, { id, name: novoNome });
		},
		[users, mutate]
	);

	if (!users) {
		return <p>Carregando...</p>;
	}

	return (
		<div>
			<ul>
				{users.map((u) => {
					return (
						<li key={u.id}>
							<Link to={`/users/${u.id}`}>{u.name}</Link>
							<button type="button" onClick={() => handleNameChange(u.id)}>
								Alterar nome
							</button>
						</li>
					);
				})}
			</ul>
		</div>
	);
};

export default UserList;
