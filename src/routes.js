import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import UserList from "./pages/UserList";
import UserDetails from "./pages/UserDetails";

const ManyUserList = () => {
	return (
		<>
			<UserList />
			<UserList />
			<UserList />
		</>
	);
};
const AppRoutes = () => {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/" exact component={ManyUserList} />
				<Route path="/users/:id" component={UserDetails} />
			</Switch>
		</BrowserRouter>
	);
};

export default AppRoutes;
